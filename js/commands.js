(function($, Drupal) {
    Drupal.AjaxCommands.prototype.reload = function(ajax, response, status){
        window.location.reload();
    }
})(jQuery, Drupal);